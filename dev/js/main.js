

$(document).ready(function() {


    // инициализация куки
    if ($.cookie('cart') == null || $.cookie('cart') == '') {
        $.cookie('cart', '');
        $.cookie('total', 0);
    }

    // инициализация корзины
    initCart();

    // плавный скролл к якорям
    $('.js-scroll').on('click', function (e) {

        e.preventDefault();
        $('body, html').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 800 );

    });

    // детальный просмотр товара
    $('.catalog-item').on('click', function () {
        var itemID = $(this).attr('data-id');

        $.ajax({
            type: 'GET',
            url: '/catalog.php',
            dataType: 'json',
            data: 'id=' + itemID,
            success: function(response) {
                buildDetailViewModal(response, itemID);
                $('#good').modal('show');
            },
            error: function() {
                console.log('error occurred');
            }
        });
    });

    // смена большого фото в детальном просмотре
    $('#item-photos').on('click', '.dv__small-photo', function () {
        var bigImgURL = $(this).attr('src');
        $('#item-primary-photo').attr('src', bigImgURL);
    });

    // обработка добавления в корзину
    $('#addToCart').on('click', function () {
        // записываю куки
        var itemID = $(this).attr('data-id');
        var itemPrice = parseInt( $(this).attr('data-price') );
        var cookieItemID = $.cookie('cart') == '' ? itemID : $.cookie('cart') + ',' + itemID;
        var cookieTotalCost = $.cookie('total') == 0 ? itemPrice : +$.cookie('total') + itemPrice;
        $.cookie('cart', cookieItemID);
        $.cookie('total', cookieTotalCost);

        // обновляю корзину
        initCart();

        $('#successfulAddToCart').show(300);
        if ( $('#emptyCart').is(':visible') ) {
            $('#emptyCart').hide(0);
        }
    });

    // удаление из корзины
    $('#modalCartTable').on('click', '.modalCart_remove', function () {
        var id = $(this).parent().attr('data-id');
        var price = $(this).parent().attr('data-price');

        // уменьшение цены при удалении
        var domCartPrice = $('.header__cart__price');
        var totalCost = parseInt(domCartPrice.text()) - price;
        domCartPrice.text(totalCost + '.00 руб.');
        $('#totalCost').text('Итого: ' + totalCost + '.00 руб.');

        // изменение куки
        $.cookie('total', totalCost);
        var cookieOldCart = $.cookie('cart').split(',');
        var cookieNewCart = [];
        cookieOldCart.forEach( function (item) {
            if (item !== id) {
                cookieNewCart.push(item);
            }
        });
        $.cookie('cart', cookieNewCart.join(','));

        // скрытие вещи
        $(this).parent().hide(300);

        // если корзина пуста
        if ($.cookie('total') == 0) {
            $('#emptyCart').show(0);
            $('#orderForm').hide(0);
            $('#order').hide(0);
        }
    });

    // при закрытии модалки детального просмотра скрываю "добавлено"
    $('#good').on('hidden.bs.modal', function () {
        $('#successfulAddToCart').hide(0);
    });

    // оформление заказа
    $('#order').on('click', function () {
        var formData = $('#orderForm').serialize();

        $.ajax({
            type: 'GET',
            url: '/order.php',
            data: formData,
            success: function() {
                $('#order').hide(0);
                $.cookie('cart', '');
                $.cookie('total', 0);
                $('#successOrder').show(500);
                $('body').on('click', function () {
                    location.href = '/';
                });
            },
            error: function() {
                $('#inputPhoneWrapper').addClass('has-error');
            }
        });
    });


});



// helpers
function initCart() {
    $('.header__cart__price').text( $.cookie('total') + '.00 руб.' );
    $('#totalCost').text( 'Итого: ' + $.cookie('total') + '.00 руб.' );

    // заполнение корзины
    if ($.cookie('total') != 0) {
        $('#emptyCart').hide(0);
        $('#modalCartTable').empty();
        if ( $('#orderForm').is(':hidden') ) {
            $('#orderForm').show(0);
        }
        if ( $('#order').is(':hidden') ) {
            $('#order').show(0);
        }

        $.ajax({
            type: 'GET',
            url: '/catalog.php',
            dataType: 'json',
            data: 'id=' + $.cookie('cart'),
            success: function(response) {
                if (response.length) {
                    response.forEach( function (item, i) {
                        buildCartModal(item, i);
                    });
                } else {
                    buildCartModal(response, 0);
                }
            },
            error: function() {}
        });
    } else {
        $('#orderForm').hide(0);
        $('#order').hide(0);
    }
}


function buildCartModal(objItem, i) {
    var ids = $.cookie('cart').split(',');
    var modalBody = $('#modalCartTable');
    modalBody.append('<tr></tr>');
    var row = $('#modalCartTable > tr:last-child');
    row.attr('data-id', ids[i]);
    row.attr('data-price', objItem.price);
    row.append('<td class="modalCart__img"><img src="/img/goods/' + objItem.img[0] + '" alt=""></td>');
    row.append('<td class="modalCart__name">' + objItem.name + '</td>');
    row.append('<td class="modalCart__price">' + objItem.price + '.00 руб.</td>');
    row.append('<td class="modalCart_remove"><i class="fa fa-trash" aria-hidden="true"></i></td>');
}


function buildDetailViewModal(objItem, id) {
    $('#item-name').text(objItem.name);
    var itemImages = objItem.img;
    $('#item-primary-photo').attr('src', '/img/goods/' + itemImages[0]);
    $('#item-photos').empty(); // удалить предыдущие фото
    itemImages.forEach( function (item) {
        $('#item-photos').append('<img src="/img/goods/' + item + '" class="dv__small-photo">');
    });
    $('#item-color').text(objItem.color);
    $('#item-cloth').text(objItem.cloth);
    $('#item-sizes').text(objItem.sizes);
    $('#item-length').text(objItem.lngth);
    $('#item-price').text(objItem.price + '.00 руб.');
    $('#addToCart').attr('data-id', id).attr('data-price', objItem.price);
}
