<?php


require_once './cat_items.php';

if ( isset($_GET['id']) && !empty($_GET['id']) ) {
    $items = explode(',', $_GET['id']);

    if ( count($items) == 1 ) {
        $response = $catalog[$items[0]];
    } else {
        $response = array();
        foreach ($items as $item) {
            $response[] = $catalog[$item];
        }
    }

    header('Content-Type: application/json');
    echo json_encode($response);

} else {
    http_response_code(400);
}
