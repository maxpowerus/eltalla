<?php


// variables
$phone = '+7 495 721-66-03';


?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Купить одежду для высоких женщин</title>

    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/img/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/favicon/favicon-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/img/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/img/favicon/favicon-16x16.png" sizes="16x16">

    <!-- STYLES -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="all" href="css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">

    <!-- JS -->
    <script src="/js/jquery-3.1.1.min.js" defer></script>
    <script src="/js/jquery.cookie.js" defer></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous" defer></script>
    <script src="/js/main.js" type="text/javascript" defer></script>

</head>
<body>

    <header class="header">
        <div class="container">
            <div class="col-md-3 col-xs-12 va_mid">
                <div class="header__contacts">
                    <div class="header__contacts__phone"><?=$phone?></div>
                    <div class="header__contacts__worktime">пн.-пт. с 9:00 до 22:00</div>
                </div>
            </div><!--
            --><div class="header__shop-name col-md-6 col-xs-12 va_mid">
                <h1 class="header__title">Одежда для высоких женщин</h1>
                <a href="/" class="font_jso">Tall elegant ladies</a>
            </div><!--
            --><div class="col-md-3 col-xs-12 va_mid">
                <div class="header__cart" id="cart" data-toggle="modal" data-target="#modalCart">
                    <div class="header__cart__img">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    </div>
                    <div class="header__cart__price">пусто</div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="goodLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Корзина</h4>
                    </div>
                    <div class="modal-body" id="modalCartBody">
                        <div id="cartBody">
                            <p id="emptyCart">Корзина пуста</p>
                            <table class="table table-responsive">
                                <tbody id="modalCartTable"></tbody>
                            </table>
                            <p id="totalCost"></p>
                        </div>

                        <form id="orderForm" class="form-horizontal" role="form">
                            <div class="row">
                                <div class="form-group col-md-12" id="inputPhoneWrapper">
                                    <label for="inputPhone" class="col-md-3 control-label">Телефон</label>
                                    <div class="col-md-9">
                                        <input type="text" name="phone" maxlength="20" class="form-control" id="inputPhone" placeholder="Номер телефона">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="inputAddress" class="col-md-3 control-label">Адрес</label>
                                    <div class="col-md-9">
                                        <textarea name="address" id="inputAddress" class="form-control" placeholder="Адрес (не заполняйте при самовывозе)"></textarea>
                                    </div>
                                </div>
                            </div>
                            <p id="successOrder">Заказ успешно оформлен. С Вами в ближайшее время свяжется наш менеджер.</p>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="modalCartButtons">
                            <button class="btn btn-eltalla" id="order">Оформить заказ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- #modalCart -->
    </header>

    <nav class="nav_wrapper hidden-xs">
        <div class="container js-scroll">
            <div class="col-md-12">
                <ul class="nav__menu__items">
                    <li><a href="#dresses" class="js-scroll">Платья</a></li>
                    <li><a href="#blouses" class="js-scroll">Блузки</a></li>
                    <li><a href="#skirts" class="js-scroll">Юбки</a></li>
                    <li><a href="#pants" class="js-scroll">Брюки</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="categories">
        <div class="container js-scroll">
            <h2>Категории</h2>
            <div class="categories__item col-md-6">
                <div class="categories__item__img categories__item__img_dresses"></div>
                <a href="#dresses" class="categories__item__anchor js-scroll">смотреть все платья</a>
            </div>
            <div class="categories__item col-md-6">
                <div class="categories__item__img categories__item__img_blouses"></div>
                <a href="#blouses" class="categories__item__anchor js-scroll">смотреть все блузки</a>
            </div>
            <div class="categories__item col-md-6">
                <div class="categories__item__img categories__item__img_skirts"></div>
                <a href="#skirts" class="categories__item__anchor js-scroll">смотреть все юбки</a>
            </div>
            <div class="categories__item col-md-6">
                <div class="categories__item__img categories__item__img_pants"></div>
                <a href="#pants" class="categories__item__anchor js-scroll">смотреть все брюки</a>
            </div>
        </div>
    </div>

    <div class="catalog">
        <h2>Каталог</h2>
        <div class="container">
            <div class="catalog-section col-md-12" id="dresses">
                <h3>Платья</h3>
                <div class="catalog-item" data-id="dr01">
                    <div class="catalog-item__img">
                        <img src="/img/goods/dress_blue/dress_blue_1.jpg"
                             alt="Платье с воротником и синей вставкой">
                    </div>
                    <div class="catalog-item__name">Платье с воротником и синей вставкой</div>
                    <div class="catalog-item__price">4 000.00 руб.</div>
                </div>
                <div class="catalog-item" data-id="dr02">
                    <div class="catalog-item__img">
                        <img src="/img/goods/dress_heart/dress_heart_1.jpg"
                             alt="Платье с аппликацией &laquo;Сердечко&raquo;">
                    </div>
                    <div class="catalog-item__name">Платье с аппликацией &laquo;Сердечко&raquo;</div>
                    <div class="catalog-item__price">3 500.00 руб.</div>
                </div>
                <div class="catalog-item" data-id="dr03">
                    <div class="catalog-item__img">
                        <img src="/img/goods/dress_horse/dress_horse_1.jpg"
                             alt="Платье с аппликацией &laquo;Конь&raquo;">
                    </div>
                    <div class="catalog-item__name">Платье с аппликацией &laquo;Конь&raquo;</div>
                    <div class="catalog-item__price">3 500.00 руб.</div>
                </div>
            </div>

            <div class="catalog-section col-md-12" id="blouses">
                <h3>Блузки</h3>
                <div class="catalog-item" data-id="bl01">
                    <div class="catalog-item__img">
                        <img src="/img/goods/blouse_blue/blouse_blue_1.jpg"
                             alt="Рубашка голубая">
                    </div>
                    <div class="catalog-item__name">Рубашка голубая</div>
                    <div class="catalog-item__price">4 000.00 руб.</div>
                </div>
                <div class="catalog-item" data-id="bl02">
                    <div class="catalog-item__img">
                        <img src="/img/goods/blouse_white/blouse_white_1.jpg"
                             alt="Рубашка белая">
                    </div>
                    <div class="catalog-item__name">Рубашка белая</div>
                    <div class="catalog-item__price">4 000.00 руб.</div>
                </div>
            </div>

            <div class="catalog-section col-md-12" id="skirts">
                <h3>Юбки</h3>
                <div class="catalog-item" data-id="sk01">
                    <div class="catalog-item__img">
                        <img src="/img/goods/skirt/skirt_1.jpg"
                             alt="Юбка-карандаш">
                    </div>
                    <div class="catalog-item__name">Юбка-карандаш</div>
                    <div class="catalog-item__price">1 500.00 руб.</div>
                </div>
            </div>

            <div class="catalog-section col-md-12" id="pants">
                <h3>Брюки</h3>
                <div class="catalog-item" data-id="pa01">
                    <div class="catalog-item__img">
                        <img src="/img/goods/pants/pants_1.jpg"
                             alt="Брюки классического силуэта">
                    </div>
                    <div class="catalog-item__name">Брюки</div>
                    <div class="catalog-item__price">2 500.00 руб.</div>
                </div>
            </div>

            <div class="modal fade" id="good" tabindex="-1" role="dialog" aria-labelledby="goodLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" id="good_modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="item-name"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-7 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12"><img src="" alt="" id="item-primary-photo"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 dv__photos" id="item-photos"></div>
                                    </div>
                                </div>
                                <div class="col-md-5 col-xs-12 dv__info">
                                    <p id="item-color"></p>
                                    <p id="item-cloth"></p>
                                    <p id="item-sizes"></p>
                                    <p id="item-length"></p>
                                    <p class="dv__price" id="item-price"></p>
                                    <button class="btn btn-eltalla" id="addToCart" data-id="" data-price="">Добавить в корзину</button>
                                    <p id="successfulAddToCart">Добавлено</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- modal #good -->

        </div>
    </div>

    <footer class="footer">
        <div class="footer__info_wrapper">
            <div class="container footer__info">
                <div class="col-md-3">
                    <p class="footer__info__item__header">О нас</p>
                    <ul class="footer__info__item">
                        <li><a href="#contacts" data-toggle="modal">Контакты</a></li>
                        <li><a href="#about" data-toggle="modal">О компании</a></li>
                        <li><a href="#bulk" data-toggle="modal">Оптовикам</a></li>
                    </ul>

                    <div class="modal fade" id="contacts" tabindex="-1" role="dialog" aria-labelledby="contactsLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Контакты</h4>
                                </div>
                                <div class="modal-body">
                                    <p><strong>Контактная информация</strong></p>
                                    <p>Контактный телефон: +7 495 721-66-03</p>
                                    <p>e-mail: sale@eltalla.ru</p>
                                    <br>
                                    <p><strong>Режим работы</strong></p>
                                    <p>Ежедневно с 9.00 до 22.00</p>
                                    <br>
                                    <p><strong>Реквизиты компании</strong></p>
                                    <p>OOО «ТЕЛЬ»</p>
                                    <p>ОГРН 5147746074306, выдано 09.09.2014 г., МИФНС №46 по г. Москве</p>
                                    <p>ИНН 7718997761</p>
                                    <p>КПП 771801001</p>
                                    <p>ОКАТО 45263552000</p>
                                    <p>ОКВЭД 52.42, 18.22, 51.42, 74.81</p>
                                    <p>Юр. адрес: 107258, г. Москва, б-р Маршала Рокоссовского, д. 36/1, оф. 40</p>
                                </div>
                            </div>
                        </div>
                    </div> <!-- modal #contacts -->

                    <div class="modal fade" id="about" tabindex="-1" role="dialog" aria-labelledby="contactsLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">О нас</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Рады Вас приветствовать на сайте Eltalla.ru!</p>
                                    <p>В нашем интернет-магазине Вы можете приобрести одежду для женщин высокого роста — от 170см до 183см.</p>
                                    <p>В настоящий момент на сайте представлена продукция польской марки iFLA. Эта компания проектирует и производит одежду специально для высоких женщин. Вся одежда выполнена из высококачественных материалов.</p>
                                    <p>В дальнейшем мы планируем расширять наш ассортимент, привлекая не только зарубежных партнеров, но и отечественных дизайнеров.</p>
                                    <p>Мы будем признательны, если Вы отправите Ваши пожелания и предложения по ассортименту одежды, а также отзывы о покупках на почту нашего сайта: sale@eltalla.ru</p>
                                </div>
                            </div>
                        </div>
                    </div> <!-- modal #about -->

                    <div class="modal fade" id="bulk" tabindex="-1" role="dialog" aria-labelledby="contactsLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Оптовикам</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Условия сотрудничества:</p>
                                    <ol>
                                        <li>Для ознакомления с ассортиментом Поставщик обеспечивает Покупателя каталогом в электронном варианте.</li>
                                        <li>Отгрузка товара Покупателю осуществляется на условиях 100% предоплаты.</li>
                                        <li>Продавец обеспечивает Покупателя информационными материалами по ознакомлению с товаром, а также методическими материалами по визуальному мерчандайзингу (бесплатно).</li>
                                        <li>Доставка товара до транспортной компании осуществляется силами и за счет Поставщика, доставка товара до пункта назначения осуществляется за счет Покупателя.</li>
                                    </ol>
                                    <p>Связаться по вопросам сотрудничества вы можете по телефону +7 495 721-66-03 или по e-mail: sale@eltalla.ru</p>
                                </div>
                            </div>
                        </div>
                    </div> <!-- modal #bulk -->
                </div>

                <div class="col-md-3">
                    <p class="footer__info__item__header">Интернет-магазин</p>
                    <ul class="footer__info__item">
                        <li><a href="#howToOrder" data-toggle="modal">Как заказать</a></li>
                        <li><a href="#payment" data-toggle="modal">Оплата</a></li>
                        <li><a href="#delivery" data-toggle="modal">Доставка</a></li>
                        <li><a href="#sizes" data-toggle="modal">Таблица размеров</a></li>
                    </ul>

                    <div class="modal fade" id="howToOrder" tabindex="-1" role="dialog" aria-labelledby="contactsLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Как заказать</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Как оформить заказ?</p>
                                    <p>Вы можете оформить заказ без регистрации, воспользовавшись корзиной заказа. Так же доступен способ оформления заказа через менеджера по телефону <?=$phone?></p>
                                    <p>Как оформить заказ без регистрации на сайте?</p>
                                    <p>Вы можете оформить заказ без регистрации на сайте. Для этого понравившиеся вещи нужно добавить в корзину. Далее выбрать способ доставки и нажать кнопку «Перейти к оплате». В открывшейся странице надо заполнить поля, обязательными являются поля «Имя» и «Телефон». После оформления заказа с вами связывается менеджер интернет-магазина и оформляет заказ с уточнением адреса доставки. Если менеджер не может дозвониться до вас в течении 3-х дней — заказ считается аннулированным.</p>
                                    <p>Как оформить заказ через менеджера по телефону?</p>
                                    <p>Для оформления заказа по телефону необходимо знать название изделия, размер и цвет. Менеджер самостоятельно оформляет заказ с указанием ваших контактных данных.</p>
                                    <p>Как внести изменения в уже оформленный заказ?</p>
                                    <p>Добавить позицию в оформленный заказ вы может во время подтверждения заказа менеджером, продиктовав ему название изделия, размер и цвет, либо позвонив на наш телефон: <?=$phone?>. Обратите внимание, что если ваш заказ подтвержден менеджером и находится в статусе «ожидает доставки», он не подлежит изменению.</p>
                                </div>
                            </div>
                        </div>
                    </div> <!-- modal #howToOrder -->

                    <div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="contactsLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Оплата</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Оплата может быть произведена только наличными по факту доставки товара курьером или при самовывозе.</p>
                                </div>
                            </div>
                        </div>
                    </div> <!-- modal #payment -->

                    <div class="modal fade" id="delivery" tabindex="-1" role="dialog" aria-labelledby="contactsLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Доставка</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Доставка по Москве и Московской области осуществляется курьерской службой в течение 2-3-х дней, включая выходные дни. Стоимость доставки по Москве — 250 рублей, по Московской области — 400 рублей.</p>
                                    <p>Вы можете указать желаемое время при оформлении заказа на сайте или согласовать с курьером уже после оплаты покупки.</p>
                                    <p>После оформления заказа с вами связывается менеджер, чтобы уточнить детали доставки.</p>
                                    <p>Отказаться от доставки или изменить время можно, позвонив по телефону <?=$phone?>.</p>
                                </div>
                            </div>
                        </div>
                    </div> <!-- modal #delivery -->

                    <div class="modal fade" id="sizes" tabindex="-1" role="dialog" aria-labelledby="contactsLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Таблица размеров</h4>
                                </div>
                                <div class="modal-body">
                                    <table class="sizes">
                                        <tr>
                                            <th>Размер</th>
                                            <th>Обхват талии</th>
                                            <th>Обхват бедер</th>
                                            <th>Обхват бюста</th>
                                        </tr>
                                        <tr>
                                            <td>36</td>
                                            <td>70-75</td>
                                            <td>86-92</td>
                                            <td>78-84</td>
                                        </tr>
                                        <tr>
                                            <td>38</td>
                                            <td>76-82</td>
                                            <td>91-98</td>
                                            <td>82-88</td>
                                        </tr>
                                        <tr>
                                            <td>40</td>
                                            <td>77-84</td>
                                            <td>95-105</td>
                                            <td>84-95</td>
                                        </tr>
                                        <tr>
                                            <td>42</td>
                                            <td>82-90</td>
                                            <td>99-108</td>
                                            <td>92-100</td>
                                        </tr>
                                        <tr>
                                            <td>44</td>
                                            <td>88-95</td>
                                            <td>104-112</td>
                                            <td>96-104</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- modal #sizes -->
                </div>

                <div class="col-md-3">
                    <p class="footer__info__item__header">Способы оплаты</p>
                    <ul class="footer__info__item">
                        <li>Наличными курьеру</li>
                    </ul>
                </div>

                <div class="col-md-3">
                    <p class="footer__info__item__header">Способы доставки</p>
                    <ul class="footer__info__item">
                        <li>Курьером</li>
                        <li>Почта России</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="footer__copy">
            <div class="container">
                <div class="col-md-12">
                    <div class="footer__copy__rights">
                        &copy;2015 eltalla.ru
                        <br>
                        Все права защищены
                    </div>
                    <a href="/" class="footer__copy__shop-name font_jso">Tall Elegant Ladies</a>
                </div>
            </div>
        </div>
    </footer>

</body>
</html>
