<?php


set_time_limit(30);
date_default_timezone_set('Europe/Moscow');

require_once './cat_items.php';
require_once './swiftmailer/lib/swift_required.php';


if ( isset($_GET['phone']) && !empty($_GET['phone']) ) {
    $items = explode(',', $_COOKIE['cart']);

    $message = 'Поступил заказ от ' . $_GET['phone'] . ' на сумму ' . $_COOKIE['total'] . ".00 руб.\n";
    $i = 1;
    foreach ($items as $item) {
        $message .= $i . '. ' . $catalog[$item]['name'] . "\n";
        $i++;
    }

    if ( isset($_GET['address']) && !empty($_GET['address']) ) {
        $message .= 'Адрес доставки: ' . $_GET['address'];
    } else {
        $message .= 'Самовывоз.';
    }

    $bytehandId = 27085;
    $bytehandKey = "C5A265AA8A604434";
    $bytehandFrom = "SMS-INFO";
    $receiver = "79037216603";

    function sendSMS($to, $text)
    {
        global $bytehandId;
        global $bytehandKey;
        global $bytehandFrom;

        $result = @file_get_contents('http://bytehand.com:3800/send?id=' . $bytehandId . '&key='.$bytehandKey . '&to='.urlencode($to).'&from=' . urlencode($bytehandFrom) . '&text='.urlencode($text));
        if ($result === false)
            return false;
        else
            return true;
    }

    $smsResponse = sendSMS($receiver, $message);

    if ($smsResponse) {
        http_response_code(200);
    } else {
        http_response_code(400);
    }
} else {
    http_response_code(400);
}
