<?php


$catalog = array(
    //dresses
    'dr01' => [
        'name' => 'Платье с воротником и синей вставкой',
        'price' => '4000',
        'color' => 'Цвет: черный-синий',
        'cloth' => 'Ткань: вискоза80%, эластан 15%, полиэстер 5%',
        'sizes' => 'Размеры: 36, 38, 40, 42',
        'lngth' => 'Длина: 108 см',
        'img' => ['dress_blue/dress_blue_1.jpg', 'dress_blue/dress_blue_2.jpg']
    ],
    'dr02' => [
        'name' => 'Платье с аппликацией "Сердечко"',
        'price' => '3500',
        'color' => 'Цвет: серый',
        'cloth' => 'Ткань: вискоза 80%, эластан 15%, полиэстер 5%',
        'sizes' => 'Размеры: 36, 38, 40, 42, 44',
        'lngth' => 'Длина: 106 см. Длина рукава: 72 см',
        'img' => ['dress_heart/dress_heart_1.jpg']
    ],
    'dr03' => [
        'name' => 'Платье с аппликацией "Конь"',
        'price' => '3500',
        'color' => 'Цвет: серый',
        'cloth' => 'Ткань: вискоза 80%, эластан 15%, полиэстер 5%',
        'sizes' => 'Размеры: 36, 38, 40, 42, 44',
        'lngth' => 'Длина: 106 см. Длина рукава: 72 см',
        'img' => ['dress_horse/dress_horse_1.jpg', 'dress_horse/dress_horse_2.jpg', 'dress_horse/dress_horse_3.jpg']
    ],

    // blouses
    'bl01' => [
        'name' => 'Рубашка голубая',
        'price' => '4000',
        'color' => 'Цвет: голубой',
        'cloth' => 'Ткань: хлопок 97%, эластан 3%',
        'sizes' => 'Размеры: 36, 38, 40, 42, 44',
        'lngth' => 'Длина по спинке: 67 см. Длина рукава: 69 см',
        'img' => ['blouse_blue/blouse_blue_1.jpg', 'blouse_blue/blouse_blue_2.jpg']
    ],
    'bl02' => [
        'name' => 'Рубашка белая',
        'price' => '4000',
        'color' => 'Цвет: белый',
        'cloth' => 'Ткань: хлопок 97%, эластан 3%',
        'sizes' => 'Размеры: 36, 38, 40, 42, 44',
        'lngth' => 'Длина по спинке: 67 см. Длина рукава: 69 см',
        'img' => ['blouse_white/blouse_white_1.jpg', 'blouse_white/blouse_white_2.jpg', 'blouse_white/blouse_white_3.jpg']
    ],

    // skirts
    'sk01' => [
        'name' => 'Юбка-карандаш',
        'price' => '1500',
        'color' => 'Цвет: черный',
        'cloth' => 'Ткань: вискоза 80%, эластан 20%',
        'sizes' => 'Размеры: 36, 38, 40, 42, 44',
        'lngth' => '',
        'img' => ['skirt/skirt_1.jpg', 'skirt/skirt_2.jpg', 'skirt/skirt_3.jpg']
    ],

    // pants
    'pa01' => [
        'name' => 'Брюки',
        'price' => '2500',
        'color' => 'Цвет: черный',
        'cloth' => 'Ткань: вискоза 80%, эластан 20%',
        'sizes' => 'Размеры: 36, 38, 40, 42, 44',
        'lngth' => 'Длина: по наружному шву — 117 см, по внутреннему шву — 91 см',
        'img' => ['pants/pants_1.jpg', 'pants/pants_2.jpg']
    ]
);
