'use strict';

var gulp = require('gulp'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch'),
    sass = require('gulp-sass'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

var path = {
    public: {
        js: 'public/js/',
        css: 'public/css/',
        img: 'public/img/'
    },

    dev: {
        js: 'dev/js/main.js',
        css: 'dev/css/main.scss',
        img: 'dev/img/**/*.*'
    },

    watch: {
        js: 'dev/js/**/*.js',
        css: 'dev/css/**/*.scss',
        img: 'dev/img/**/*.*'
    }
};

gulp.task('js:build', function() {
    gulp.src(path.dev.js)
        .pipe(uglify())
        .pipe(gulp.dest(path.public.js));
});

gulp.task('css:build', function() {
    gulp.src(path.dev.css)
        .pipe(sass())
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(gulp.dest(path.public.css));
});

gulp.task('image:build', function() {
    gulp.src(path.dev.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.public.img));
});

gulp.task('build', [
    'js:build',
    'css:build',
    'image:build'
]);

gulp.task('watch', function() {
    watch([path.watch.css], function() {
        gulp.start('css:build');
    });
    watch([path.watch.js], function() {
        gulp.start('js:build');
    });
    watch([path.watch.img], function() {
        gulp.start('image:build');
    });
});

gulp.task('default', ['build', 'watch']);